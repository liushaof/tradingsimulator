QT += sql widgets charts

CONFIG += c++11

HEADERS += \
    bougie.h \
    businesstrade.h \
    filesoperation.h \
    simulator.h \
    trading.h

SOURCES += \
    bougie.cpp \
    filesoperation.cpp \
    main.cpp \
    simulator.cpp \
    trading.cpp
