#ifndef FILESOPERATION_H
#define FILESOPERATION_H
#include "bougie.h"
/*!
 * \brief This file  defines methods handling loading of csv files and getting all csv files to load from current folder.
 */
void readcsvfile(const QString path, EvolutionCours& e);
void getAllFileFolder(QString dirPath, std::vector<QString> &folder);
#endif // CSVLOAD_H
