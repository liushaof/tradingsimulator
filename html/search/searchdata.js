var indexSectionsWithContent =
{
  0: "_abcdefghilmnoprstuvz~",
  1: "bcdehpst",
  2: "bfmst",
  3: "abcdefghilmoprstuv~",
  4: "_bcdefhilmnoprstuvz",
  5: "ci",
  6: "bt",
  7: "abfmrs",
  8: "d"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "related"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Friends"
};

