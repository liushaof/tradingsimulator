var searchData=
[
  ['getallfilefolder',['getAllFileFolder',['../filesoperation_8cpp.html#a95c35a23e068a427949b66bdcbc8315e',1,'getAllFileFolder(QString dirPath, std::vector&lt; QString &gt; &amp;folder):&#160;filesoperation.cpp'],['../filesoperation_8h.html#a95c35a23e068a427949b66bdcbc8315e',1,'getAllFileFolder(QString dirPath, std::vector&lt; QString &gt; &amp;folder):&#160;filesoperation.cpp']]],
  ['getbase',['getBase',['../class_paire_devises.html#a6878818a7170f53fef760a57341b2216',1,'PaireDevises']]],
  ['getclose',['getClose',['../class_cours_o_h_l_c.html#a8e4b301ff74f2477fc362f297ba9bd7b',1,'CoursOHLC']]],
  ['getcode',['getCode',['../class_devise.html#a65c5b31c2e59217f55adf2fd75719150',1,'Devise']]],
  ['getcontrepartie',['getContrepartie',['../class_paire_devises.html#a57f9fdb0cb7ec461f2448babfdebfd65',1,'PaireDevises']]],
  ['getcoursohlc',['getCoursOHLC',['../class_bougie.html#a659ddfcc926c225615fb96f5f3e69700',1,'Bougie::getCoursOHLC()'],['../class_bougie.html#a77c3c522529f26876126b760fd72147b',1,'Bougie::getCoursOHLC() const']]],
  ['getdate',['getDate',['../class_cours_o_h_l_c.html#ab9db40e8053399bf6c588b9e9a6cc62b',1,'CoursOHLC']]],
  ['getdevise',['getDevise',['../class_devises_manager.html#a1cb1bd72b5d0f049820774225efae829',1,'DevisesManager']]],
  ['getdisplaytrades',['getDisplayTrades',['../class_simulator.html#adec717d896a217db768699ae9531fea1',1,'Simulator']]],
  ['gethigh',['getHigh',['../class_cours_o_h_l_c.html#acce98cb49c00531505d04a4fbc8cffea',1,'CoursOHLC']]],
  ['getinfo',['getInfo',['../class_trading_exception.html#a94f9ff6dc6b02b4f24e39783ff438881',1,'TradingException']]],
  ['getlow',['getLow',['../class_cours_o_h_l_c.html#abcf95cab9611e1dc6a6b6786679fd9b9',1,'CoursOHLC']]],
  ['getmanager',['getManager',['../class_devises_manager.html#a271ad1137d23b6ef3779110a806af778',1,'DevisesManager']]],
  ['getmonnaie',['getMonnaie',['../class_devise.html#aee1c3a78a47e7a59d93097711e8041ee',1,'Devise']]],
  ['getnbcours',['getNbCours',['../class_evolution_cours.html#aa8abf863c4a64622bf46d201b5aa691e',1,'EvolutionCours']]],
  ['getnextdate',['getNextDate',['../class_evolution_cours.html#af78501f6e5b04f9b8069c6f3ab5506b0',1,'EvolutionCours']]],
  ['getopen',['getOpen',['../class_cours_o_h_l_c.html#af8893a044fc497e94efa17589fdfd505',1,'CoursOHLC']]],
  ['getopenpricebydate',['getOpenPriceByDate',['../class_evolution_cours.html#afbf7155686bddec82f2b3a95fb44cb7d',1,'EvolutionCours']]],
  ['getpairedevises',['getPaireDevises',['../class_evolution_cours.html#a8ef59cffc428acd73bb0d8631a1b54e3',1,'EvolutionCours::getPaireDevises()'],['../class_devises_manager.html#a86f555706b9f5b9170920944e4ba1e6e',1,'DevisesManager::getPaireDevises()']]],
  ['getrsi',['getRsi',['../class_simulator.html#ac02f1032b6c8cfaaad129b68142531b8',1,'Simulator']]],
  ['getsourceend',['getSourceEnd',['../class_evolution_cours.html#acaadf047567e84abaa0e72a3b7b82717',1,'EvolutionCours']]],
  ['getsourcestart',['getSourceStart',['../class_evolution_cours.html#aa20b7b14f6b27538af4817bc249a113f',1,'EvolutionCours']]],
  ['getsurnom',['getSurnom',['../class_paire_devises.html#ab71e900ae5692748434143a89beb4761',1,'PaireDevises']]],
  ['getvolume',['getVolume',['../class_cours_o_h_l_c.html#a47bdd652e36b57002399167438065c66',1,'CoursOHLC']]],
  ['getzonegeographique',['getZoneGeographique',['../class_devise.html#a265989ed393c22311d7f57118c358f1e',1,'Devise']]]
];
