var searchData=
[
  ['nbcours',['nbCours',['../class_evolution_cours.html#ade277051feae16d6c4a8b084084bbcb6',1,'EvolutionCours']]],
  ['nbdevises',['nbDevises',['../class_devises_manager.html#a684f8713148c514340be9c93e2b24b78',1,'DevisesManager']]],
  ['nbmaxcours',['nbMaxCours',['../class_evolution_cours.html#a4175704b98163446cd7f7529b3255735',1,'EvolutionCours']]],
  ['nbmaxdevises',['nbMaxDevises',['../class_devises_manager.html#a86b329bbea57446e4ede57573eae47cc',1,'DevisesManager']]],
  ['nbmaxpaires',['nbMaxPaires',['../class_devises_manager.html#a76a816d4ebf24801db751dab877833ca',1,'DevisesManager']]],
  ['nbpaires',['nbPaires',['../class_devises_manager.html#a5477932c2e25bca9b6511b1cf475ecad',1,'DevisesManager']]],
  ['nextpage',['nextPage',['../class_simulator.html#ae10462e40ad77832d2f69703c1971b6d',1,'Simulator']]],
  ['notes',['notes',['../structbusinesstrade.html#a98388d5d46e47d8db36f7a5e8c156eb1',1,'businesstrade::notes()'],['../class_simulator.html#ad1341e4c097df784552647463afd309f',1,'Simulator::notes()']]]
];
