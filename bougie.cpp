#include "bougie.h"
/*!
 * \brief Creation of a candle in graph.
 * \param open Open price.
 * \param high High price.
 * \param low Low price.
 * \param close Close price.
 * \param c CoursOHLC to be handled.
 * \param timestamp defineits form.
 * \param parent Parent widget.
 */
Bougie::Bougie(qreal open, qreal high, qreal low, qreal close, CoursOHLC* c, qreal timestamp, QObject* parent):
QCandlestickSet(open, high, low, close, timestamp, parent), cours(c){
    connect(this,SIGNAL(hovered(bool)),this, SLOT(viewCoursOHLC(bool)));
    info = new QWidget();
    layout = new QVBoxLayout();
    openl = new QLabel();
    highl = new QLabel();
    lowl = new QLabel();
    closel = new QLabel();
    datel = new QLabel();
    volumel = new QLabel();
    datel->setText("Date : " + c->getDate().toString("dd.MM.yyyy"));
    openl->setText("Open : " + QString::number(open));
    highl->setText("High : " + QString::number(high));
    lowl->setText("Low : " + QString::number(low));
    closel->setText("Close : " + QString::number(close));
    volumel->setText("Volume : " + QString::number(c->getVolume()));
    info->setLayout(layout);
    layout->addWidget(openl);
    layout->addWidget(highl);
    layout->addWidget(highl);
    layout->addWidget(closel);
    layout->addWidget(volumel);
    layout->addWidget(datel);
    info->setWindowFlags(Qt::Widget | Qt::FramelessWindowHint | Qt::ToolTip | Qt::WindowStaysOnTopHint);
    info->setAttribute(Qt::WA_NoSystemBackground, true);
    info->setAttribute(Qt::WA_TranslucentBackground, true);
}
/*!
 * \brief Display the information of each candle when cursor moves on it.
 * \param show bool coming from the signal hovered() to tell if the cursor is on the candle.
 * \return None.
 */
void Bougie::viewCoursOHLC(bool show) const{
    QPoint p = info->mapFromGlobal(QCursor::pos());
    info->move(p.x(), p.y());
    if(show)
        info->show();
    else
        info->hide();
}
