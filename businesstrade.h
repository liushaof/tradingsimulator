#ifndef BUSINESSTRADE_H
#define BUSINESSTRADE_H
#include "trading.h"

enum businessStatus
{
	finished,
	repeal,
};

enum businessDirection
{
	buyd,
	selld,
};

enum tradeModel
{
	autox, 
	manual,
	stepbystep,
};
/*!
 * \brief This file contains the struct of each trading record.
 * \param i id
 * \param c cost of a trading
 * \param d direction: buy or sell
 * \param s status: usually finished
 * \param dt date of this trade
 * \param r roi
 * \param cc cc2 for debug
 * \param u u for debug
 * \param nt notes for this trade
 */
struct businesstrade
{
    businesstrade(int i, double c, businessDirection d, businessStatus s, QDate dt, QString r, double cc, double u, QString nt)
	{
		id = i;
		cost = c;
		direction = d;
		status = s;
		date = dt;
        roi = r;
		cc2 = cc;
		unit = u;
		notes = nt;
	}
	int id;
	double cost;
	businessDirection direction;
	businessStatus status;
	QDate date;
    QString roi;
	double cc2;
	double unit;
	QString notes;
};

#endif
