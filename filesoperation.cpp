#include "bougie.h"
/*!
 * \brief Loading datas in a csv file to a EvolutionCours.
 * \param pathx Path of the csv file.
 * \param e EvolutionCours to be handled.
 */
void readcsvfile(const QString pathx, EvolutionCours& e){
    QString path = "./files/" + pathx + ".csv";
    qDebug()<<path;
    QFile csvFile(path);
	if (e.hasLoaded(pathx))
	{
		return;
	}
    QStringList csvList;
    csvList.clear();
    // Open the file
    if (csvFile.open(QIODevice::ReadWrite)){
        QTextStream stream(&csvFile);
        while (!stream.atEnd()){
            // Save it into the list
            csvList.push_back(stream.readLine());
        }
    csvFile.close();
    }
    else{
        QMessageBox::about(NULL, "Csv file error", "Can not open the file!");
    }
    int i = 0;
    unsigned int indexo = 0, indexh = 0, indexd = 0, indexl = 0, indexc = 0, indexv = 0;
    Q_FOREACH(QString str, csvList){
        ++i;
        if(i == 1) {
            QStringList namesplit = str.split(",");
            for (unsigned int j = 0; j < namesplit.size(); j++){
                if (namesplit[j].toLower() == "date")
                    indexd = j;
                else if (namesplit[j].toLower() == "open")
                    indexo = j;
                else if (namesplit[j].toLower() == "high")
                    indexh = j;
                else if (namesplit[j].toLower() == "close")
                    indexc = j;
                else if (namesplit[j].toLower() == "low")
                    indexl = j;
                else if (namesplit[j].toLower() == "volume")
                    indexv = j;
                else
                    continue;
            }
        }
        if(i > 1){
            // Split the string
            QStringList valsplit = str.split(",");       
            QDate Date = QDate::fromString(valsplit[indexd],"yyyy-MM-dd");
            e.addCours(valsplit[indexo].toDouble(),valsplit[indexh].toDouble(),valsplit[indexc].toDouble(),valsplit[indexc].toDouble(),valsplit[indexv].toInt(),Date);
        }
    }
}
/*!
 * \brief Getting all csv files' names which are in the folder dirPath.
 * \param dirPath the folder's name.
 * \param folder names of csv files.
 */
void getAllFileFolder(QString dirPath, std::vector<QString> &folder)

{

    QDir* dir = new QDir(dirPath);
    QStringList filter;
    filter<<"*.csv";
    dir->setNameFilters(filter);
    QFileInfoList fileInfoList=dir->entryInfoList(filter);
    for (int i=0;i<fileInfoList.count();i++)
        {
            folder.push_back(fileInfoList[i].fileName().split('.')[0]);
        }


}


