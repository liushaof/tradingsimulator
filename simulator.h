#ifndef Simulator_H
#define Simulator_H
#include "bougie.h"
#include "businesstrade.h"
#include "filesoperation.h"
#include <map>
#include <vector>
#include <QSqlQuery>
#include <QToolBar>
#include <QPushButton>
#include <QDateEdit>
#include <QTableWidget>
#include <QLineSeries>
#include <QtSql/QSqlDatabase>
#include <QTimer>

class Bougie;
/*!
 * \brief This class holds the only one interface of trading simulator, nearly all functions are implemented here.
 */
class Simulator : public QWidget{
    Q_OBJECT
    EvolutionCours& evolution;
    QCandlestickSeries* candle_series; // un ensemble de bougies
    QLineSeries* ema;
    QLineSeries* ema1;
    std::vector<qreal> emav;
    std::vector<qreal> emav26;
    QLineSeries* rsi;
    std::vector<qreal> rsiv;
    std::vector<qreal> closev;
    QLineSeries* macd;
    std::vector<qreal> macdv;
    QChart *candle_chart; // un graphique sur un ensemble de bougies
    QChart *bar_chart;
    QChart *rsi_chart;
    QChart *macd_chart;
    QChartView *candle_chartView; // un viewer de graphique
    QChartView *bar_chartView;
    QChartView *rsi_chartView;
    QChartView *macd_chartView;
    QBarSeries* bar_series;
    QVBoxLayout* fenetre;
    QToolBar* tools;
    QDateEdit* startdate;
    QDateEdit* enddate;
    QPushButton* btnSelectDate;

    QLabel*		currencyLabelX;
    QComboBox* currencysCombo;
    QPushButton* btnChoiceCurrency;
    QStringList currlists;


    QComboBox* modelCombo;
    QPushButton* btnChoiceModel;
    QStringList modelList;

    QPushButton* btnPurchaseAndSales;
    QLabel* selectTimeTip;

    QLabel* initalbaseLabel;
    QLineEdit* intialbaseCurrencyEdit;
    double basecurrency = 0.0;
    double curbase=0.0;
    QLabel* initalcounterLabel;
    QLineEdit* initialcounterCurrencyEdit;
    double countercurrency = 1000000.0;
    double curcount = 1000000.0;
    QLabel* brokerfeeLabel;
    QLineEdit* brokerfeeEdit;
    QPushButton* btnSetIntialCurrency;
    double brokerfee = 0.1; //0.1%(from 0.1 to 100) (0.1% to 100%)
    double brokerbaseprofit=0.0;
    double brokercountprofit = 0.0;
    QWidget* charts;
    unsigned int drawCount=0;

    QLabel* curbaseProfitLabel;
    QLineEdit* curbaseProfit;
    QLabel* curcounterProfitLabel;
    QLineEdit* curcounterProfit;

    QLabel* baseProfitLabel;
    QLineEdit* baseProfit;
    QLabel* counterProfitLabel;
    QLineEdit* counterProfit;

    //add buy panel
    QLabel* buyDateLabel;
    QDateEdit* buydateEdit;
    QLabel* buyCostLabel;
    QLineEdit*	buyCostEdit;
    QPushButton* buyBtn;

    //add sell panel
    QLabel* sellDateLabel;
    QDateEdit* selldateEdit;
    QLabel* sellCostLabel;
    QLineEdit*	sellCostEdit;
    QPushButton* sellBtn;

    //add bussiness list
    QTableWidget* bussiness;
    QPushButton* prevPage;
    QPushButton* nextPage;
    QPushButton* deleteLastTrade;
    QLabel* bussniessLabel;
    QStringList headers;

    int curtradeid = 0;
    int pageSize = 5;
    int curPageIndex = 0;
    std::map<int, businesstrade/*, std::greater<int>*/> cbusines;
    std::map<int, QString> statusList;
    std::map<int, QString> directList;

    QDateTime previosTradeTime;
    QLabel* title_notes;
    QTextEdit* notes;
    QPushButton* btnSave;
    QPushButton* btnLoad;

    std::vector<QString> folder;
    bool bHasDelLast;
public:
    explicit Simulator(EvolutionCours& e,QWidget *parent = nullptr);
    void updateUI();
    void fillTradeLog();
    std::map<int, businesstrade> getDisplayTrades();
    signals:
private slots:
protected slots:
    void triggerStartEndDate();
    void triggerSetInitial();
    void triggerBuy();
    void triggerSell();
    void triggerChangeCurrency();
    void triggerNextPage();
    void triggerPrevPage();
    void triggerDeleteLastTrade();
    void triggerSave();
    void triggerLoad();
    void triggerSetModel();
    void triggerAutoTrade();
public slots:
private:
    void cleanCharts();
    void updateCurrencys();
    bool isCanTrade();
    void initLogic();
    QString curCurrency;
    qreal getRsi(qreal curclose);
    void initDb();
    void createTables();
    void cleanTable();
    QSqlDatabase _db;
    tradeModel _model;
    QTimer* _timer;
    unsigned int _drawCount;
};


#endif
