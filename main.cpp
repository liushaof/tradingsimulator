#include <QApplication>
#include "trading.h"
#include "simulator.h"
#include "filesoperation.h"
/*!
 * \brief main function which initializes the trading simulator with needed params.
 */
int main(int argc, char* argv[]) {
    DevisesManager& m=DevisesManager::getManager();
    m.creationDevise("INT", "Init Currency","Init");
    m.creationDevise("CNT", "Counter Currency","Counter");
    const PaireDevises& INT_CNT=m.getPaireDevises("INT", "CNT");
    EvolutionCours e(INT_CNT);
    QApplication app(argc, argv);
    Simulator t(e);
    t.show();
    return app.exec();
}
