#ifndef BOUGIE_H
#define BOUGIE_H
#include <QWidget>
#include <QtCharts>
#include <QDate>
#include "trading.h"
/*!
     * @brief This class represents the candle of each day in the graph.
     */
class Bougie: public QCandlestickSet{
    Q_OBJECT
    CoursOHLC* cours;
    QVBoxLayout* layout;
    QLabel* openl;
    QLabel* highl;
    QLabel* lowl;
    QLabel* closel;
    QLabel* volumel;
    QLabel* datel;
    QWidget* info;
        public:
        Bougie(qreal open, qreal high, qreal low, qreal close,CoursOHLC* c, qreal
        timestamp = 0.0, QObject *parent = nullptr);
        CoursOHLC& getCoursOHLC() { return *cours; }
        const CoursOHLC& getCoursOHLC() const { return *cours; }
        signals:
        private slots:
        void viewCoursOHLC(bool show) const;
};

#endif // BOUGIE_H
