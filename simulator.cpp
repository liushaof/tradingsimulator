#include "Simulator.h"
/*!
 * \brief Constructor of the graphic interface of Trading simulator.
 * \param e EvolutionCours to be handled.
 * \param parent parentWidget
 */
Simulator::Simulator(EvolutionCours& e, QWidget *parent)
    :QWidget(parent), evolution(e) {
    bHasDelLast = false;
    _model = manual;
    _drawCount = 60;
    initDb();
    qDebug()<<"start initLogic";
    initLogic();
    createTables();
}
/*!
 * \brief Constructor of the graphic interface of Trading simulator.
 * \details Each time users select or modify the information of the trading simulation, this function will update the graphe to display the information that users want. The information are candles, EMA, RSI, MACD and volume.
 *  */
void Simulator::updateUI()
{
    cleanCharts();

    drawCount = 0;
    QBarSet* bar = new QBarSet("Volume", this);
    QStringList categories;
    double ymin = evolution.fbegin()->getLow();
    double ymax = evolution.fbegin()->getHigh();
    //double vmin = evolution.fbegin()->getVolume();
    double vmax = evolution.fbegin()->getVolume();
    for (EvolutionCours::iterator it = evolution.fbegin(); it != evolution.fend(); ++it) {
        CoursOHLC& cours = *it;
        Bougie* bougie = new Bougie(cours.getOpen(), cours.getHigh(), cours.getLow(), cours.getClose(), &cours);
        if (bougie->getCoursOHLC().getHigh() >= ymax)
        {
            ymax = bougie->getCoursOHLC().getHigh();
        }
        if (bougie->getCoursOHLC().getLow() <= ymin)
        {
            ymin = bougie->getCoursOHLC().getLow();
        }
        *bar << cours.getVolume();
        candle_series->append(bougie);
        if (vmax < cours.getVolume())
        {
            vmax = cours.getVolume();
        }
        //qDebug()<<cours.getDate().toString("dd.MM.yyyy");
        categories << cours.getDate().toString("dd.MM.yyyy");

        qreal curema = bougie->open();
        qreal curema26 = bougie->open();
        qreal diffx = bougie->open();
        qreal curmacd = bougie->open();
        qreal currsi = 0.0;
        if (drawCount != 0)
        {
            curema = (*(emav.rbegin())) * 11 / 13 + 2 * bougie->open() / 13;
            curema26 = (*(emav.rbegin())) * 25 / 27 + 2 * bougie->open() / 27;
            diffx = curema - curema26;
            curmacd = (*(macdv.rbegin())) * 8 / 10 + diffx * 2 / 10;
            currsi = getRsi(bougie->open());

        }
        emav.push_back(curema);
        emav26.push_back(curema26);
        macdv.push_back(curmacd);
        rsiv.push_back(currsi);
        ema->append(drawCount, curema);
        ema1->append(drawCount, curema);
        macd->append(drawCount ,curema26);
        rsi->append(drawCount, currsi);

        if (ymax < curema)
        {
            ymax = curema;
        }
        if (ymin > curema)
        {
            ymin = curema;
        }
        if (ymax < curema26)
        {
            ymax = curema26;
        }
        if (ymin > curema26)
        {
            ymin = curema26;
        }

        if (ymax < curmacd)
        {
            ymax = curmacd;
        }
        if (ymin > curmacd)
        {
            ymin = curmacd;
        }

        ++drawCount;


        if (drawCount == _drawCount)
        {
            break;
        }
        closev.push_back(bougie->open());
    }
    QBarCategoryAxis* candle_axisX = new QBarCategoryAxis();
    candle_axisX->append(categories);
    QValueAxis* candle_axisY = new QValueAxis();
    candle_axisY->setMax(qreal(ymax));
    candle_axisY->setMin(qreal(ymin));
    unsigned int width = (drawCount+20) * 100;
    charts->resize(width, 1000);
    candle_chart->addSeries(candle_series);
    candle_chart->addSeries(ema);
    candle_chart->setTitle("Price");
    candle_chart->createDefaultAxes();
    candle_chart->setAxisX(candle_axisX, candle_series);
    candle_chart->setAxisY(candle_axisY, candle_series);
    candle_chart->setAxisY(candle_axisY, ema);
    candle_chart->setAnimationOptions(QChart::SeriesAnimations);
    candle_chart->legend()->setAlignment(Qt::AlignBottom);
    candle_chart->legend()->setVisible(true);

    QBarCategoryAxis* rsi_axisX = new QBarCategoryAxis();
    rsi_axisX->append(categories);

    QValueAxis* rsi_axisY = new QValueAxis();
    rsi_axisY->setMax(qreal(100.0));
    rsi_axisY->setMin(qreal(0.0));

    rsi_chart->addSeries(rsi);
    rsi_chart->createDefaultAxes();
    rsi_chart->setAxisX(rsi_axisX, rsi);
    rsi_chart->setAxisY(rsi_axisY, rsi);

    rsi_chart->setAnimationOptions(QChart::SeriesAnimations);
    rsi_chart->legend()->setAlignment(Qt::AlignBottom);
    rsi_chart->legend()->setVisible(true);

    bar_series->append(bar);

    QValueAxis* volume_axisY = new QValueAxis();
    volume_axisY->setMax(qreal(vmax));
    volume_axisY->setMin(qreal(0.0));

    QBarCategoryAxis* bar_axisX = new QBarCategoryAxis();

    bar_axisX->append(categories);
    bar_chart->addSeries(bar_series);
    bar_chart->removeAxis(bar_chart->axisX());
    bar_chart->createDefaultAxes();
    bar_chart->setAxisX(bar_axisX, bar_series);
    bar_chart->setAxisY(volume_axisY, bar_series);
    bar_chart->setAnimationOptions(QChart::SeriesAnimations);
    bar_chart->legend()->setAlignment(Qt::AlignBottom);
    bar_chart->legend()->setVisible(true);


    QBarCategoryAxis* macd_axisX = new QBarCategoryAxis();
    macd_axisX->append(categories);
    QValueAxis* macd_axisY = new QValueAxis();
    macd_axisY->setMax(qreal(ymax));
    macd_axisY->setMin(qreal(ymin));
    macd_chart->addSeries(macd);
    macd_chart->addSeries(ema1);
    macd_chart->createDefaultAxes();
    macd_chart->setAxisX(macd_axisX, macd);
    macd_chart->setAxisY(macd_axisY, macd);
    macd_chart->setAxisY(macd_axisY, ema1);
    macd_chart->setAnimationOptions(QChart::SeriesAnimations);
    macd_chart->legend()->setAlignment(Qt::AlignBottom);
    macd_chart->legend()->setVisible(true);

}
/*!
 * \brief Generating the trading records.
 * \details Each time users buy or sell or delete last trade, this function will be called to generate the trading records. These records display in a Table.
 *  */
void Simulator::fillTradeLog()
{
    bussiness->clear();
    bussiness->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    bussiness->setColumnCount(headers.size());
    bussiness->setHorizontalHeaderLabels(headers);
    auto displays = getDisplayTrades();
    bussiness->setRowCount((int)displays.size());
    int index = 0;
    for (const auto& log : displays)
    {
        bussiness->setItem(index, 0, new QTableWidgetItem(QString::number(log.second.id+1)));
        bussiness->setItem(index, 1, new QTableWidgetItem(QString::number(log.second.cost)));
        bussiness->setItem(index, 2, new QTableWidgetItem(log.second.date.toString()));
        bussiness->setItem(index, 3, new QTableWidgetItem(directList[log.second.direction]));
        bussiness->setItem(index, 4, new QTableWidgetItem(statusList[log.second.status]));
        bussiness->setItem(index, 5,new QTableWidgetItem(log.second.roi));
        bussiness->setItem(index, 6, new QTableWidgetItem(log.second.notes));
        ++index;
    }
    updateCurrencys();
}
/*!
 * \brief Getting all trading records.
 * \details \ref  fillTradeLog uses this function to get all history records. These records are stored in a hashmap with a id and a struct bussiness trade.
 * \return ret type std::map.
 *  */
std::map<int, businesstrade> Simulator::getDisplayTrades()
{
    std::map<int, businesstrade> ret;
    auto it = cbusines.find(curPageIndex);
    if (it == cbusines.end())
    {
        return ret;
    }
    int hascount = 0;
    while (it != cbusines.end() && hascount++ < pageSize)
    {
        ret.insert(std::make_pair(it->first, it->second));
        ++it;
    }
    return ret;
}
/*!
 * \brief Trigger of button choice date to select the start and the end date users want to trade.
 * \details Once users select our date, the button choice date will be disabled to prevent user from modifing dates. \ref updateUI will be called after selecting dates.
 *  */
void Simulator::triggerStartEndDate()
{
    if (!startdate || !enddate)
    {
        return;
    }
    QDate startd = startdate->date();
    QDate endd = enddate->date();
    if (startd > endd)
    {
        return;
    }
    if (_model != stepbystep && _model!=autox)
    {
        if (startd.addDays(drawCount) < endd)
        {
            endd = startd.addDays(drawCount);
        }
    }
    enddate->setDate(endd);
    evolution.setStartEndDate(startd, endd);

    startdate->setEnabled(false);
    if (_model == stepbystep)
    {
        enddate->setEnabled(true);
        btnSelectDate->setEnabled(true);
        if (startdate->date().addDays(25) < enddate->date())
        {
            startdate->setDate(enddate->date().addDays(-25));
        }
    }
    else
    {
        enddate->setEnabled(false);
        btnSelectDate->setEnabled(false);
    }

    updateUI();
    if (_model == stepbystep)
    {
        buydateEdit->setDate(evolution._lastEfectiveDate);
        selldateEdit->setDate(evolution._lastEfectiveDate);
    }
    else
    {
        buydateEdit->setDate(startd);
        selldateEdit->setDate(startd);
    }
}
/*!
 * \brief Trigger of button setInitialCurrency to set the base currency and counter currency and broker fee.
 * \details Once users set these parameters of trading, this button will be disabled to prevent users from modifing them.
 *  */
void Simulator::triggerSetInitial()
{
    double defaultzero = 0.000001;
    basecurrency = intialbaseCurrencyEdit->text().toDouble();
    countercurrency = initialcounterCurrencyEdit->text().toDouble();
    brokerfee = brokerfeeEdit->text().toDouble();
    if (basecurrency < defaultzero && countercurrency < defaultzero)
    {
        basecurrency = 0.0;
        countercurrency = 1000000;
    }
    if (brokerfee >= 100.0 || brokerfee < defaultzero)
    {
        brokerfee = 0.1;
    }
    curbase = basecurrency;
    curcount = countercurrency;
    btnSetIntialCurrency->setEnabled(false);
    intialbaseCurrencyEdit->setEnabled(false);
    initialcounterCurrencyEdit->setEnabled(false);
    brokerfeeEdit->setEnabled(false);
}
/*!
 * \brief Trigger of buy button used in manual mode and step by step mode.
 * \details Once users buy a currency, the record will be added to the table and the information of currency users hold will be changed.
 *  */
void Simulator::triggerBuy()
{   if(btnSelectDate->isEnabled() || btnChoiceCurrency->isEnabled() || btnSetIntialCurrency->isEnabled() || btnChoiceModel->isEnabled()){
        if(_model != stepbystep	){
            QMessageBox::about(NULL, "Buy error", "Please set your date, currencies and start information.");
                return;
        }

    }
    double buyCost = buyCostEdit->text().toDouble();
    QDate bdate = buydateEdit->date();
    double buyUnit = evolution.getOpenPriceByDate(bdate);
    if ( bdate < startdate->date() || bdate > enddate->date()){
        QMessageBox::about(NULL, "Buy error", "Can not buy for the date selected.");
          return;
    }
    if (curbase < buyCost || buyUnit < 0.0000001 || !isCanTrade() )
    {
        return;
    }
    curbase -= buyCost;
    double tempcounter = buyCost * (100 - brokerfee) * buyUnit/100.0;
    brokerbaseprofit += buyCost * brokerfee / 100.0;
    curcount += tempcounter;
    businesstrade trade(curtradeid++, buyCost, buyd, finished, bdate, QString::number((curbase+curcount)/(basecurrency+countercurrency)), tempcounter, buyUnit, notes->toPlainText());
    cbusines.insert(std::make_pair(trade.id, trade));
    fillTradeLog();
    previosTradeTime = QDateTime::currentDateTime();
    bHasDelLast = false;
    notes->clear();
}
/*!
 * \brief Trigger of sell button used in manual mode and step by step mode.
 * \details Once users sell a currency, the record will be added to the table and the information of currency users hold will be changed.
 *  */
void Simulator::triggerSell()
{   if(btnSelectDate->isEnabled() || btnChoiceCurrency->isEnabled() || btnSetIntialCurrency->isEnabled()){
        if(_model != stepbystep	){
            QMessageBox::about(NULL, "Buy error", "Please set your date, currencies and start information.");
                return;
        }
    }
    double sellCost = sellCostEdit->text().toDouble();
    QDate sdate = selldateEdit->date();
    double sellUnit = evolution.getOpenPriceByDate(sdate);
    if(sdate < startdate->date() || sdate > enddate->date()){
       QMessageBox::about(NULL, "Sell error", "Can not sell for the date selected.");
         return;
    }
    if (curcount < sellCost || sellUnit < 0.0000001 || !isCanTrade())
    {
        return;
    }
    curcount -= sellCost;
    brokercountprofit += sellCost * brokerfee / 100.0;
    double tempcounter = sellCost - sellCost * brokerfee / 100.0;
    double tempbase = tempcounter / sellUnit;
    curbase += tempbase;
    businesstrade trade(curtradeid++, sellCost, selld, finished, sdate, QString::number((curbase+curcount)/(basecurrency+countercurrency)), tempbase, sellUnit, notes->toPlainText());
    cbusines.insert(std::make_pair(trade.id, trade));
    fillTradeLog();
    previosTradeTime = QDateTime::currentDateTime();
    bHasDelLast = false;
    notes->clear();

}
/*!
 * \brief Trigger of sure button to select the file that users want to load.
 * \details The file selected contains the currency information that users need to trade. And the file's name let us know that two currency users want to trade.
 *  */
void Simulator::triggerChangeCurrency()
{
    QString combo = currencysCombo->currentText();
    QString path = "./files/" + combo + ".csv";
    evolution.cleanCours();
    readcsvfile(combo, evolution);
    updateUI();
    btnChoiceCurrency->setEnabled(false);
    currencysCombo->setEnabled(false);
    curCurrency = combo;
    initDb();
}
/*!
 * \brief Trigger of nextPage button.
 * \details By clicking the button, users will see more trading record.
 *  */
void Simulator::triggerNextPage()
{
    if (curPageIndex + pageSize < cbusines.size())
    {
        curPageIndex += pageSize;
        fillTradeLog();
    }
}
/*!
 * \brief Trigger of prevPage button.
 * \details By clicking the button, users will see more previous trading records.
 *  */
void Simulator::triggerPrevPage()
{
    if (curPageIndex - pageSize >= 0)
    {
        curPageIndex -= pageSize;
        fillTradeLog();
    }
}
/*!
 * \brief Trigger of deleteTrade button.
 * \details By clicking the button, the last trade record will be delete.
 *  */
void Simulator::triggerDeleteLastTrade(){
    if(bussiness->rowCount() > 0 && !bHasDelLast){
        businesstrade trade = cbusines.rbegin()->second;
        switch (trade.direction)
        {
        case buyd: //incr base, decr counter
        {
            countercurrency -= trade.cc2;
            curbase += trade.cc2 / trade.unit;
        }break;
        case selld: //incr counter, decr base
        {
            countercurrency += trade.cc2 * trade.unit;
            curbase -= trade.cc2;
        }break;
        default:
            break;
        }
        bussiness->removeRow(bussiness->rowCount()-1);
        cbusines.erase(--curtradeid);
        bHasDelLast = true;
        curbaseProfit->setText(QString::number(curbase));
        curcounterProfit->setText(QString::number(countercurrency));
    }


}
/*!
 * \brief Trigger of save button.
 * \details By clicking the button, the current trading records will be saved in local sql database.
 *  */
void Simulator::triggerSave()
{
    cleanTable();
    QString sql("insert into %1 (ID,cost,datex,direction,status,ROI,Notes, cc, u) values(");// '%2,%3,%4,%5,%6,%7,%8);");
    for (const auto& trade : cbusines)
    {
        auto c = trade.second;
        QString execSql = sql.arg(curCurrency);
        execSql += QString::number(c.id) + "," + QString::number(c.cost) + ",'" + c.date.toString() + "'," + QString::number(c.direction) + "," + QString::number(c.status) + ",'" + c.roi + "','" + c.notes + "', " + QString::number(c.cc2) + "," + QString::number(c.unit)  + ");";
        _db.exec(execSql);
    }
}
/*!
 * \brief Trigger of load button.
 * \details By clicking the button, the history trading records will be loaded to the table.
 * \warning If users have done a trading simulation with the pair of currency A, the history records will be loaded only if users have selected A as initial currency.
 *  */
void Simulator::triggerLoad()
{
    //cleanTable();
    bussiness->clear();
    cbusines.clear();
    QString sql("select ID,cost,datex,direction,status,ROI,Notes, cc, u from %1;");
    QString execSql = sql.arg(curCurrency);
    QSqlQuery sql_query;
    sql_query.prepare(execSql);
    if (!sql_query.exec())
    {
        qDebug() << "load failed";
    }
    else
    {
        while (sql_query.next())
        {
            int id = sql_query.value(0).toInt();
            double cost = sql_query.value(1).toDouble();
            QString datax = sql_query.value(2).toString();
            auto direction = businessDirection(sql_query.value(3).toInt());
            auto status = businessStatus(sql_query.value(4).toInt());
            QString roi = sql_query.value(5).toString();
            QString notes = sql_query.value(6).toString();
            double cc = sql_query.value(7).toDouble();
            double u = sql_query.value(8).toDouble();
            businesstrade b(id, cost, direction, status, QDate::fromString(datax), roi, cc, u, notes);
            cbusines.insert(std::make_pair(id, b));
            fillTradeLog();
        }
    }
    curPageIndex = 0;
    fillTradeLog();
}
/*!
 * \brief Trigger of submit button.
 * \details By clicking the button, users can select which mode that they want to use for this simulation. For the manual mode, the max difference between start and end is 60 days.
 *  */
void Simulator::triggerSetModel()
{
    if(btnSelectDate->isEnabled() || btnChoiceCurrency->isEnabled() || btnSetIntialCurrency->isEnabled()){
            QMessageBox::about(NULL, "Mode error", "Please set your date, currencies and start information.");
                    return;
        }

    _model = (tradeModel)(modelCombo->currentIndex());
    btnChoiceModel->setEnabled(false);
    switch (_model)
    {
    case autox: //auto model, increase current day in every one second
    {
        auto ds = evolution.getSourceStart();
        auto de = evolution.getSourceEnd();
        startdate->setDate(evolution.getSourceStart());
        enddate->setDate(evolution.getSourceEnd());
        _timer = new QTimer(this);
        _timer->setInterval(1500);
        connect(_timer, SIGNAL(timeout()), this, SLOT(triggerAutoTrade()));
        _timer->start();

    }break;
    case manual:
        break;
    case stepbystep: //step by step
    {
        startdate->hide();
        btnSelectDate->setEnabled(true);
        enddate->setEnabled(true);
        enddate->setDate(startdate->date().addDays(1));
        triggerStartEndDate();
        //evolution.setStartEndDate(startdate->date(), startdate->date());
    }break;
    default:
        break;
    }
}
/*!
 * \brief Trigger being actived when users select auto mode.
 * \details Each second a trading will be done by using RSI.
 *  */
void Simulator::triggerAutoTrade()
{

    auto ds = evolution.getSourceStart();
    auto de = evolution.getSourceEnd();
    qDebug() << "timer trigger";
    auto datex = startdate->date();
    auto datey = evolution.getNextDate(datex);
    startdate->setDate(datey);
    triggerStartEndDate();
    btnSelectDate->setEnabled(false);
    btnChoiceCurrency->setEnabled(false);
    btnSetIntialCurrency->setEnabled(false);
    buyBtn->setEnabled(false);
    sellBtn->setEnabled(false);
    buydateEdit->setEnabled(false);
    selldateEdit->setEnabled(false);
    buyCostEdit->setEnabled(false);
    sellCostEdit->setEnabled(false);
    if (startdate->date() == enddate->date()){
        _timer->stop();
        QMessageBox::about(NULL, "End auto trading", "Auto trading stops.");
        return;
    };
    auto sdate = selldateEdit->date();
    auto curopen = evolution.getOpenPriceByDate(sdate);
    auto currsi = getRsi(curopen);
    if (currsi > 80.0)
    {
        triggerSell();
    }
    else if(currsi<20.0)
    {
        triggerBuy();
    }
    else
    {
        //do nothing
    }
}
/*!
 * \brief Cleaning charts.
 * \details This function is called in \ref updateUI.
 *  */
void Simulator::cleanCharts()
{
    candle_series->clear();
    ema->clear();
    rsi->clear();
    macd->clear();
    ema1->clear();
    bar_series->clear();
}
/*!
 * \brief Updating currencies information.
 *  */
void Simulator::updateCurrencys()
{
    curbaseProfit->setText(QString::number(curbase));
    baseProfit->setText(QString::number(curbase - basecurrency));

    curcounterProfit->setText(QString::number(curcount));
    counterProfit->setText(QString::number(curcount - countercurrency));
}
/*!
 * \brief Users can buy and sell each 1 sec.
 *  */
bool Simulator::isCanTrade()
{
    QDateTime dt = QDateTime::currentDateTime();
    if (dt < previosTradeTime.addSecs(1))
    {
        return false;
    }
    return true;
}
/*!
 * \brief Initialization of graphic interface.
 * \details Display candle series of each CoursOHLC. Calculation  of EMA and MACD and display them as two line in candle chart. Display volume information in a bar chart. Display RSI variation in a chart.
 *  */
void Simulator::initLogic()
{

    getAllFileFolder("./files/", folder);
    for(unsigned int i = 0; i< folder.size(); i++)
        qDebug()<<folder.at(i);
    readcsvfile(folder.at(0),evolution);
    curCurrency = folder.at(0);
    initDb();
    resize(1800, 1200);
    drawCount = 0;

    emav.clear();
    rsiv.clear();
    macdv.clear();
    closev.clear();

    candle_series = new QCandlestickSeries(this);
    candle_series->setIncreasingColor(QColor(Qt::green));
    candle_series->setDecreasingColor(QColor(Qt::red));
    ema = new QLineSeries(this);
    ema->setName("EMA");
    ema1 = new QLineSeries(this);
    ema1->setName("EMA26");
    rsi = new QLineSeries(this);
    rsi->setName("RSI");
    macd = new QLineSeries(this);
    macd->setName("EMA10");

    bar_series = new QBarSeries();
    QBarSet* bar = new QBarSet("Volume", this);

    double ymax = 0.0;
    double ymin = evolution.begin()->getLow();

    QStringList categories;
    for (EvolutionCours::iterator it = evolution.begin(); it != evolution.end(); ++it) {
        CoursOHLC& cours = *it;
        Bougie* bougie = new Bougie(cours.getOpen(), cours.getHigh(), cours.getLow(), cours.getClose(), &cours);
        if (bougie->getCoursOHLC().getHigh() >= ymax)
        {
            ymax = bougie->getCoursOHLC().getHigh();
        }
        if (bougie->getCoursOHLC().getLow() <= ymin)
        {
            ymin = bougie->getCoursOHLC().getLow();
        }
        *bar << cours.getVolume();
        candle_series->append(bougie);

        //qDebug()<<cours.getDate().toString("dd.MM.yyyy");
        categories << cours.getDate().toString("dd.MM.yyyy");

        qreal curema = bougie->open();
        qreal curema26 = bougie->open();
        qreal diffx = bougie->open();
        qreal curmacd = bougie->open();
        qreal currsi = 0.0;
        if(drawCount!=0)
        {
            curema = (*(emav.rbegin())) * 11 / 13 + 2 * bougie->open() / 13;
            curema26 = (*(emav.rbegin())) * 25 / 27 + 2 * bougie->open() / 27;
            diffx = curema - curema26;
            curmacd = (*(macdv.rbegin())) * 8 / 10 + diffx * 2 / 10;
            currsi = getRsi(bougie->open());

        }
        emav.push_back(curema);
        emav26.push_back(curema26);
        macdv.push_back(curmacd);
        rsiv.push_back(currsi);
        ema->append(drawCount, curema);
        ema1->append(drawCount, curema);
        macd->append(drawCount ,curema26);
        rsi->append(drawCount, currsi);

        if (ymax < curema)
        {
            ymax = curema;
        }
        if (ymin > curema)
        {
            ymin = curema;
        }
        if (ymax < curema26)
        {
            ymax = curema26;
        }
        if (ymin > curema26)
        {
            ymin = curema26;
        }

        if (ymax < curmacd)
        {
            ymax = curmacd;
        }
        if (ymin > curmacd)
        {
            ymin = curmacd;
        }

        ++drawCount;


        if (drawCount == _drawCount)
        {
            break;
        }
        closev.push_back(bougie->open());
    }
    bar_series->append(bar);

    QBarCategoryAxis* candle_axisX = new QBarCategoryAxis();
    candle_axisX->append(categories);


    QValueAxis* candle_axisY = new QValueAxis();
    candle_axisY->setMax(qreal(ymax));
    candle_axisY->setMin(qreal(ymin));



    //Un graphe peut contenir plusieurs series de données.
    candle_chart = new QChart();
    candle_chart->addSeries(candle_series);
    candle_chart->addSeries(ema);
    candle_chart->setTitle("Price");
    candle_chart->createDefaultAxes();
    candle_chart->setAxisX(candle_axisX, candle_series);
    candle_chart->setAxisY(candle_axisY, candle_series);
    //candle_chart->setAxisX(candle_axisX, ema);
    candle_chart->setAxisY(candle_axisY, ema);
    //candle_chart->setAxisX(candle_axisX, macd);
    //candle_chart->setAxisY(candle_axisY, macd);


    candle_chart->setAnimationOptions(QChart::SeriesAnimations);
    candle_chart->legend()->setAlignment(Qt::AlignBottom);
    candle_chart->legend()->setVisible(true);
    candle_chartView = new QChartView(candle_chart, this);

    candle_chartView->setMinimumSize(1600, 300);

    QBarCategoryAxis* bar_axisX = new QBarCategoryAxis();
    bar_axisX->append(categories);
    bar_chart = new QChart();
    bar_chart->addSeries(bar_series);
    bar_chart->createDefaultAxes();
    bar_chart->setAxisX(bar_axisX, bar_series);

    bar_chart->setAnimationOptions(QChart::SeriesAnimations);
    bar_chart->legend()->setAlignment(Qt::AlignBottom);
    bar_chart->legend()->setVisible(true);
    bar_chartView = new QChartView(bar_chart, this);
    bar_chartView->setMinimumSize(1600, 300);

    QBarCategoryAxis* rsi_axisX = new QBarCategoryAxis();
    rsi_axisX->append(categories);

    QValueAxis* rsi_axisY = new QValueAxis();
    rsi_axisY->setMax(qreal(100.0));
    rsi_axisY->setMin(qreal(0.0));

    rsi_chart = new QChart();
    rsi_chart->addSeries(rsi);
    rsi_chart->createDefaultAxes();
    rsi_chart->setAxisX(rsi_axisX, rsi);
    rsi_chart->setAxisY(rsi_axisY, rsi);

    rsi_chart->setAnimationOptions(QChart::SeriesAnimations);
    rsi_chart->legend()->setAlignment(Qt::AlignBottom);
    rsi_chart->legend()->setVisible(true);
    rsi_chartView = new QChartView(rsi_chart, this);
    rsi_chartView->setMinimumSize(1600, 300);


    QBarCategoryAxis* macd_axisX = new QBarCategoryAxis();
    macd_axisX->append(categories);

    QValueAxis* macd_axisY = new QValueAxis();
    macd_axisY->setMax(qreal(ymax));
    macd_axisY->setMin(qreal(ymin));

    macd_chart = new QChart();
    macd_chart->addSeries(macd);
    macd_chart->addSeries(ema1);
    macd_chart->createDefaultAxes();
    macd_chart->setAxisX(macd_axisX, macd);
    macd_chart->setAxisY(macd_axisY, macd);
    macd_chart->setAxisY(macd_axisY, ema1);
    macd_chart->setAnimationOptions(QChart::SeriesAnimations);
    macd_chart->legend()->setAlignment(Qt::AlignBottom);
    macd_chart->legend()->setVisible(true);
    macd_chartView = new QChartView(macd_chart, this);
    macd_chartView->setMinimumSize(1800, 300);

    QGridLayout* charts_layout = new QGridLayout();

    charts_layout->addWidget(candle_chartView, 0, 0);
    charts_layout->addWidget(bar_chartView, 1, 0);
    charts_layout->addWidget(rsi_chartView, 2, 0);
    charts_layout->addWidget(macd_chartView, 3, 0);

    //add choice start and end date control
    startdate = new QDateEdit(evolution.getSourceStart(), this);
    enddate = new QDateEdit(evolution.getSourceEnd(), this);
    btnSelectDate = new QPushButton("choice Date", this);
    QString tips = "start:[" + evolution.getSourceStart().toString() + "],end:[" + evolution.getSourceEnd().toString() + "]";
    selectTimeTip = new QLabel(tips, this);
    QObject::connect(btnSelectDate, SIGNAL(pressed()), this, SLOT(triggerStartEndDate()));

    currencysCombo = new QComboBox(this);

    for(unsigned int i = 0; i < folder.size(); i++){
        currlists.append(folder.at(i));
    }
    currencysCombo->addItems(currlists);

    currencyLabelX = new QLabel("currency choice:", this);
    btnChoiceCurrency = new QPushButton("sure", this);
    QObject::connect(btnChoiceCurrency, SIGNAL(pressed()), this, SLOT(triggerChangeCurrency()));

    //add model select panel
    modelList.clear();
    modelList.append("auto");
    modelList.append("manual");
    modelList.append("stepbystep");
    modelCombo = new QComboBox( this);
    modelCombo->addItems(modelList);
    btnChoiceModel = new QPushButton("submit", this);

    QObject::connect(btnChoiceModel, SIGNAL(pressed()), this, SLOT(triggerSetModel()));


    //add set currency buttons
    initalbaseLabel = new QLabel("initial base:", this);
    intialbaseCurrencyEdit = new QLineEdit("0", this);
    initalcounterLabel = new QLabel("initial counter:", this);
    initialcounterCurrencyEdit = new QLineEdit("1000000", this);

    brokerfeeLabel = new QLabel("broker fee(0.1%):", this);
    brokerfeeEdit = new QLineEdit("0.1", this);
    btnSetIntialCurrency = new QPushButton("setInitialcurrency", this);

    curbaseProfitLabel = new QLabel("current base currency :", this);
    curbaseProfit = new QLineEdit(QString::number(curbase), this);
    curbaseProfit->setEnabled(false);
    curcounterProfitLabel = new QLabel("current counter currency :", this);
    curcounterProfit = new QLineEdit(QString::number(countercurrency), this);
    curcounterProfit->setEnabled(false);

    baseProfitLabel = new QLabel("base currency profit:", this);
    baseProfit = new QLineEdit(QString::number(curbase - basecurrency), this);
    baseProfit->setEnabled(false);
    counterProfitLabel = new QLabel("counter currency profit:", this);
    counterProfit = new QLineEdit(QString::number(curcount - countercurrency), this);
    counterProfit->setEnabled(false);

    QObject::connect(btnSetIntialCurrency, SIGNAL(pressed()), this, SLOT(triggerSetInitial()));

    //add buy panel
    buyDateLabel = new QLabel("buy date:", this);
    buydateEdit = new QDateEdit(evolution.getSourceStart(), this);
    //buydateEdit->setEnabled(false);
    buyCostLabel = new QLabel("buy cost currency cost:", this);
    buyCostEdit = new QLineEdit("1.0", this);
    buyBtn = new  QPushButton("buy", this);
    QObject::connect(buyBtn, SIGNAL(pressed()), this, SLOT(triggerBuy()));

    //add sell panel
    sellDateLabel = new QLabel("sell date:", this);
    selldateEdit = new QDateEdit(evolution.getSourceStart(), this);
    //selldateEdit->setEnabled(false);
    sellCostLabel = new QLabel("sell cost currency cost:", this);
    sellCostEdit = new QLineEdit("1.0", this);
    sellBtn = new  QPushButton("sell", this);
    QObject::connect(sellBtn, SIGNAL(pressed()), this, SLOT(triggerSell()));

    // area to take note for current trading
    title_notes = new QLabel("notes for this trading");
    notes = new QTextEdit(this);

    bussiness = new QTableWidget(this);
    headers.append("ID");
    headers.append("cost");
    headers.append("date");
    headers.append("direction");
    headers.append("status");
    headers.append("ROI");
    headers.append("Notes");


    bussniessLabel = new QLabel("bussiness log", this);

    auto pagingBtnWidget = new QWidget(this);
    prevPage = new QPushButton("prevPage", this);
    nextPage = new QPushButton("nextPage", this);
    deleteLastTrade = new QPushButton("deleteTrade", this);
    btnSave = new QPushButton("save", this);
    btnLoad = new QPushButton("load", this);

    QGridLayout* pagingBtnLayout = new QGridLayout();
    pagingBtnLayout->addWidget(bussniessLabel, 0, 0);
    pagingBtnLayout->addWidget(prevPage, 1, 0);
    pagingBtnLayout->addWidget(nextPage, 2, 0);
    pagingBtnLayout->addWidget(deleteLastTrade, 3, 0);
    pagingBtnLayout->addWidget(btnSave, 4, 0);
    pagingBtnLayout->addWidget(btnLoad, 5, 0);
    pagingBtnWidget->setLayout(pagingBtnLayout);

    QObject::connect(prevPage, SIGNAL(pressed()), this, SLOT(triggerPrevPage()));
    QObject::connect(nextPage, SIGNAL(pressed()), this, SLOT(triggerNextPage()));
    QObject::connect(deleteLastTrade, SIGNAL(pressed()), this, SLOT(triggerDeleteLastTrade()));
    QObject::connect(btnSave, SIGNAL(pressed()), this, SLOT(triggerSave()));
    QObject::connect(btnLoad, SIGNAL(pressed()), this, SLOT(triggerLoad()));

    charts = new QWidget();
    charts->setLayout(charts_layout);
    QScrollArea* scrollarea = new QScrollArea();
    unsigned int width = drawCount * 100;
    charts->resize(width, 1000);
    scrollarea->setWidget(charts);


    //add start date and end date choice
    QGridLayout* showTime_layout = new QGridLayout();
    showTime_layout->addWidget(startdate, 0, 0);
    showTime_layout->addWidget(enddate, 0, 1);
    showTime_layout->addWidget(btnSelectDate, 0, 2);
    showTime_layout->addWidget(selectTimeTip, 0, 3);
    showTime_layout->addWidget(currencyLabelX, 0, 4);
    showTime_layout->addWidget(currencysCombo, 0, 5);
    showTime_layout->addWidget(btnChoiceCurrency, 0, 6);
    showTime_layout->addWidget(modelCombo, 0, 7);
    showTime_layout->addWidget(btnChoiceModel, 0, 8);


    //initial currency layout
    showTime_layout->addWidget(initalbaseLabel, 1, 0);
    showTime_layout->addWidget(intialbaseCurrencyEdit, 1, 1);
    showTime_layout->addWidget(initalcounterLabel, 1, 2);
    showTime_layout->addWidget(initialcounterCurrencyEdit, 1, 3);
    showTime_layout->addWidget(brokerfeeLabel, 1, 4);
    showTime_layout->addWidget(brokerfeeEdit, 1, 5);
    showTime_layout->addWidget(btnSetIntialCurrency, 1, 6);


    //add show current currency
    showTime_layout->addWidget(curbaseProfitLabel, 2, 0);
    showTime_layout->addWidget(curbaseProfit, 2, 1);
    showTime_layout->addWidget(curcounterProfitLabel, 2, 2);
    showTime_layout->addWidget(curcounterProfit, 2, 3);

    //add show current currency profit
    showTime_layout->addWidget(baseProfitLabel, 3, 0);
    showTime_layout->addWidget(baseProfit, 3, 1);
    showTime_layout->addWidget(counterProfitLabel, 3, 2);
    showTime_layout->addWidget(counterProfit, 3, 3);

    QWidget* showStartTime = new QWidget();
    showStartTime->setLayout(showTime_layout);

    //add purchase and sales info
    showTime_layout->addWidget(buyDateLabel, 4, 0);
    showTime_layout->addWidget(buydateEdit, 4, 1);
    showTime_layout->addWidget(buyCostLabel, 4, 2);
    showTime_layout->addWidget(buyCostEdit, 4, 3);
    showTime_layout->addWidget(buyBtn, 4, 4);

    QObject::connect(buyBtn, SIGNAL(pressed()), this, SLOT(triggerBuy()));

    showTime_layout->addWidget(sellDateLabel, 5, 0);
    showTime_layout->addWidget(selldateEdit, 5, 1);
    showTime_layout->addWidget(sellCostLabel, 5, 2);
    showTime_layout->addWidget(sellCostEdit, 5, 3);
    showTime_layout->addWidget(sellBtn, 5, 4);

    QObject::connect(sellBtn, SIGNAL(pressed()), this, SLOT(triggerSell()));

    showTime_layout->addWidget(bussiness, 6, 0, 1, 6);
    showTime_layout->addWidget(pagingBtnWidget, 6, 6);
    showTime_layout->addWidget(title_notes, 2, 5);
    showTime_layout->addWidget(notes, 3, 5, 3, 2);



    QGridLayout* fenetre = new QGridLayout();
    fenetre->addWidget(scrollarea);
    fenetre->addWidget(showStartTime);
    this->setLayout(fenetre);
    statusList.insert(std::make_pair(finished, "finished"));
    statusList.insert(std::make_pair(repeal, "repeal"));
    directList.insert(std::make_pair(buyd, "buy in"));
    directList.insert(std::make_pair(selld, "sell out"));

    update();

    previosTradeTime = QDateTime::currentDateTime();

    fillTradeLog();
    updateCurrencys();
    qDebug()<<"init logic success";
}
/*!
 * \brief getRsi indicator of each day.
 * \param curclose current close price.
 * \return RSI indicator.
 *  */
qreal Simulator::getRsi(qreal curclose)
{
    size_t n = 7;
    size_t actual1 = 0;
    size_t actual2 = 0;
    qreal accum1 = 0;
    qreal accum2 = 0;
    qreal temp = curclose;
    for (auto it = closev.rbegin(); it != closev.rend(); ++it)
    {
        accum1 += (temp - *it) > 0.00001 ? (temp - *it) : 0.0;
        temp = *it;
        ++actual1;
        if (actual1 >= n)break;
    }
    temp = curclose;
    for (auto it = closev.rbegin(); it != closev.rend(); ++it)
    {
        accum2 += (temp - *it) > 0.0000001 ? (temp - *it) : (*it-temp);
        temp = *it;
        ++actual2;
        if (actual2 >= n)break;
    }
    if (actual2 == 0)
    {
        return 0.0;
    }
    return (accum1 / actual1) / (accum2 / actual2) * 100;
}
/*!
 * \brief Initialization of database.
 *  */
void Simulator::initDb()
{
    _db = QSqlDatabase::addDatabase("QSQLITE");
    _db.setDatabaseName("./db.db");
    if (!_db.open())
    {
        qDebug() << "create database failed";
    }
    qDebug() << "create database success";
}
/*!
 * \brief Creation of tables for each pair of currency.
 *  */
void Simulator::createTables()
{
    QString sql("create table %1 (ID int, cost double, datex varchar(30), direction int, status int, ROI varchar(20), Notes varchar(200), cc double, u double);");
    for (const auto& table : folder)
    {
        QString execSql = sql.arg(table);
        _db.exec(execSql);
    }
    _db.commit();
}
/*!
 * \brief Cleaning a table.
 *  */
void Simulator::cleanTable()
{
    QString sql("delete from %1;");
    QString execSql = sql.arg(curCurrency);
    _db.exec(execSql);
    _db.commit();
}

