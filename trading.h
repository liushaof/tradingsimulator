#ifndef TRADING_H
#define TRADING_H
#include <QString>
#include <QDate>
#include<cmath>
#include <map>
#include <memory>
using namespace std;
/*!
 * \brief Class Exception.
 */
class TradingException {
public:
    TradingException(const QString& message) :info(message) {}
    QString getInfo() const { return info; }
private:
    QString info;
};
/*!
 * \brief Class currency.
 */
class Devise {
    QString code;
    QString monnaie, zone;
    Devise(const QString& c, const QString& m, const QString& z = "");
    ~Devise() = default;
    friend class DevisesManager;
public:
    QString getCode() const { return code; }
    const QString& getMonnaie() const { return monnaie; }
    const QString& getZoneGeographique() const { return zone; }
};
/*!
 * \brief Class pair of currency.
 */
class PaireDevises {
    const Devise* base;
    const Devise* contrepartie;
    QString surnom;
    PaireDevises(const Devise& b, const Devise& c, const QString& s = "");
    ~PaireDevises() = default;
    friend class DevisesManager;
public:
    const Devise& getBase() const { return *base; }
    const Devise& getContrepartie() const { return *contrepartie; }
    const QString& getSurnom() const { return surnom; }
    QString toString() const;
};
/*!
 * \brief Class reprensenting information of each candle.
 */
class CoursOHLC {
    double open = 0, high = 0, low = 0, close = 0;
    int volume = 0;
    QDate date;
public:
    CoursOHLC() {}
    CoursOHLC(double o, double h, double l, double c, int v, const QDate& d);
    double getOpen() const { return open; }
    double getHigh() const { return high; }
    double getLow() const { return low; }
    double getClose() const { return close; }
    int getVolume() const {return volume; }
    void setCours(double o, double h, double l, double c, int v);
    QDate getDate() const { return date; }
    void setDate(const QDate& d) { date=d; }

	bool isDateEqual(const QDate& d2) { return d2 == date; }
	bool isDateLess(const QDate& d2) { return date < d2; }
	bool isDateMore(const QDate& d2) { return date > d2; }
};
/*!
 * \brief A series of candles.
 */
class EvolutionCours {
    const PaireDevises* paire;
    CoursOHLC* cours = nullptr;
    unsigned int nbCours = 0;
    unsigned int nbMaxCours = 0;
	std::shared_ptr<QDate> startdate;
	std::shared_ptr<QDate> enddate;
	
	std::map<QString, CoursOHLC*> mapCours;
public:
	QDate _lastEfectiveDate;

	EvolutionCours(const PaireDevises& p) :paire(&p) { startdate = nullptr; enddate = nullptr; }
    void addCours(double o, double h, double l, double c, int v, const QDate& d);
    /*!
     * \brief Cleanning cours of this Evolution Cours.
     */
	void cleanCours()
	{
		delete[] cours;
		cours = 0;
		nbCours = 0;
		nbMaxCours = 0;
	}
    /*!
     * \brief Cleanning cours of this Evolution Cours.
     */
	bool hasLoaded(QString path)
	{
		auto it = mapCours.find(path);
		if (it != mapCours.end())
		{
			cours = it->second;
			return true;
		}
		else
		{
			return false;
		}
	}

	bool setStartEndDate(QDate startd, QDate endd)
	{
		if (startd > endd)
		{
			return false;
		}
		if (startd < getSourceStart())
		{
			startd = getSourceStart();
		}
		if (endd > getSourceEnd())
		{
			endd = getSourceEnd();
		}
		startdate = std::make_shared<QDate>(startd);
		enddate = std::make_shared<QDate>(endd);
		return true;
	}

	double getOpenPriceByDate(QDate date)
	{
		double ret = 0;
		for (unsigned int i = 0; i < nbCours; ++i)
		{
			if ((cours + i)->isDateEqual(date))
			{
				ret = (cours + i)->getOpen();
				break;
			}
		}
		return ret;
	}

	QDate getNextDate(const QDate& d)
	{
        for (unsigned int i = 0; i < nbCours; ++i)
		{
			if ((cours + i)->getDate() > d)
			{
				return (cours + i)->getDate();
			}
		}
		return cours->getDate();
	}

	QDate getSourceStart()
	{
		return cours->getDate();
	}
	QDate getSourceEnd()
	{
		return (cours + nbCours - 1)->getDate();
	}

    ~EvolutionCours();
    EvolutionCours(const EvolutionCours& e);
    EvolutionCours& operator=(const EvolutionCours& e);
    unsigned int getNbCours() const { return nbCours; }
    const PaireDevises& getPaireDevises() const { return *paire; }
    using iterator = CoursOHLC*;
    iterator begin() { return iterator(cours); }
    iterator end() { return iterator(cours + nbCours); }
	iterator fbegin()
	{
		if (!startdate || !enddate)
		{
			return iterator(cours);
		}
		for (unsigned int i = 0; i < nbCours; ++i)
		{
			if ((cours + i)->isDateMore(*startdate) || (cours + i)->isDateEqual(*startdate))
			{
                return iterator(cours + i);
			}
		}
		return iterator(cours);
	}

	iterator fend()
	{
		if (!startdate || !enddate)
		{
			return iterator(cours+50);
		}
		for (unsigned int i = nbCours - 1; i >=0; --i)
		{
			if ((cours + i)->isDateLess(*enddate) || (cours + i)->isDateEqual(*enddate))
			{

				return iterator(cours + i);
			}
		}
		return iterator(cours+nbCours);
	}

    using const_iterator = const CoursOHLC*;
    const_iterator begin() const { return cours; }
    const_iterator end() const { return cours + nbCours; }
	const_iterator fbegin() const
	{
		if (!startdate || !enddate)
		{
			return cours;
		}
		for (unsigned int i = 0; i < nbCours; ++i)
		{
			if ((cours + i)->isDateMore(*startdate) || (cours + i)->isDateEqual(*startdate))
			{
				return (cours + i);
			}
		}
		return const_iterator(cours);
	}

	const_iterator fend() const
	{
		if (!startdate || !enddate)
		{
			return cours;
		}
		for (unsigned int i = nbCours - 1; i >=0; --i)
		{
			if ((cours + i)->isDateLess(*enddate) || (cours + i)->isDateEqual(*enddate))
			{
				return cours + i;
			}
		}
		return const_iterator(cours + nbCours);
	}
    const_iterator cbegin() const { return cours; }
    const_iterator cend() const { return cours + nbCours; }
	const_iterator fcbegin() const
	{
		if (!startdate || !enddate)
		{
			return cours;
		}
		for (unsigned int i = 0; i < nbCours; ++i)
		{
			if ((cours + i)->isDateMore(*startdate) || (cours + i)->isDateEqual(*startdate))
			{
				return (cours + i);
			}
		}
		return const_iterator(cours);
	}

	const_iterator fcend() const
	{
		if (!startdate || !enddate)
		{
			return cours;
		}
		for (unsigned int i = nbCours-1; i >= 0; --i)
		{
			if ((cours + i)->isDateLess(*enddate) || (cours + i)->isDateEqual(*enddate))
			{
				return cours + i;
			}
		}
		return const_iterator(cours + nbCours);
	}

};
/*!
 * \brief Desgin pattern Singleton.
 */
class DevisesManager {

    Devise** devises = nullptr; // tableau de pointeurs sur objets Devise
    unsigned int nbDevises = 0;
    unsigned int nbMaxDevises = 0;
    mutable PaireDevises** paires = nullptr; // tableau de pointeurs sur objets PaireDevises
    mutable unsigned int nbPaires = 0;
    mutable unsigned int nbMaxPaires = 0;
    // empêher la duplication par recopie ou affectation
    DevisesManager(const DevisesManager& m) = delete;
    DevisesManager& operator=(const DevisesManager& m) = delete;
    // pour le singleton
    DevisesManager() {}
    ~DevisesManager();
    struct Handler {
        DevisesManager* instance = nullptr;
        ~Handler() { delete instance; }
    };
    static Handler handler;
public:
    static DevisesManager& getManager() {
        if (handler.instance == nullptr)
            handler.instance = new DevisesManager;
        return *handler.instance;
    }
    static void libererManager() {
        delete handler.instance;  handler.instance = nullptr;
    }
    const Devise& creationDevise(const QString& c, const QString& m,
        const QString& z);
    const Devise& getDevise(const QString& c)const;
    const PaireDevises& getPaireDevises(const QString & c1,
        const QString & c2) const;
};
#endif // TRADING_H
